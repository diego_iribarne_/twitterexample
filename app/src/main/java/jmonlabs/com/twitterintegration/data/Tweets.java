package jmonlabs.com.twitterintegration.data;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

public class Tweets {
        List<Tweet> tweet;

        public Tweets(List<Tweet> tweet) {
            this.tweet = tweet;
        }

        public List<Tweet> getTweet() {
            return tweet;
        }
    }