package jmonlabs.com.twitterintegration.home;

import com.twitter.sdk.android.core.models.Tweet;

import java.util.List;

/**
 * Created by diegoiribarne on 29/6/16.
 */
public interface HomeContract {
    interface View {
        void showTweets(List<Tweet> tweets);

        void showProgress(Boolean progress);
    }

    interface UserActionsListener {
        void searchTweets(String tag);

    }
}
