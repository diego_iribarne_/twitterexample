package jmonlabs.com.twitterintegration.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import jmonlabs.com.twitterintegration.R;
import jmonlabs.com.twitterintegration.data.MyTwitterApiClient;
import jmonlabs.com.twitterintegration.data.Tweets;


public class HomeFragment extends Fragment implements HomeContract.View {

    private static final String WELCOME_SEARCH = "cats";
    HomeContract.UserActionsListener userActionsListener;
    RecyclerView list;
    ProgressBar loading;
    MyAdapter adapter;
    final String KEEP_ROTATION_DATA = "keepRotationData";

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = new SearchView(((AppCompatActivity) (getActivity())).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                userActionsListener.searchTweets(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (adapter != null)
            outState.putString(KEEP_ROTATION_DATA, new Gson().toJson(new Tweets(adapter.getData())).toString());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        list = (RecyclerView) root.findViewById(R.id.tweetList);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setHasFixedSize(true);
        loading = (ProgressBar) root.findViewById(R.id.loading);
        userActionsListener = new HomePresenter(new MyTwitterApiClient(Twitter.getSessionManager().getActiveSession()), this);
        Boolean previousDataExists = savedInstanceState != null && !TextUtils.isEmpty(savedInstanceState.getString(KEEP_ROTATION_DATA));
        if (previousDataExists) {
            String data = savedInstanceState.getString(KEEP_ROTATION_DATA);
            Tweets tweets = new Gson().fromJson(data, Tweets.class);
            adapter = new MyAdapter(tweets.getTweet());
            list.setAdapter(adapter);
        } else
            userActionsListener.searchTweets(WELCOME_SEARCH);
        return root;
    }


    @Override
    public void showTweets(List<Tweet> tweets) {
        adapter = new MyAdapter(tweets);
        list.setAdapter(adapter);
    }

    @Override
    public void showProgress(Boolean progress) {
        loading.setVisibility(progress ? View.VISIBLE : View.INVISIBLE);
    }

    private class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

        private List<Tweet> tweets;

        private static final int NORMAL = 2;
        private static final int HEADER = 1;
        private static final int NO_HEADER = 3;
        private static final int EMPTY = 4;

        public MyAdapter() {
            tweets = new ArrayList<>();
        }

        public void refreshData(List<Tweet> tweets) {
            this.tweets = tweets;
            this.notifyDataSetChanged();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public View tweetView;
            public TextView title;
            public TextView tweetDescription;
            public TextView tweetPublishDate;
            public ImageView userProfileImage;
            public ImageView tweetImage;

            public ViewHolder(View v) {
                super(v);
                this.tweetView = v;
                this.title = (TextView) v.findViewById(R.id.title);
                this.tweetDescription = (TextView) v.findViewById(R.id.tweet_item_description);
                this.tweetPublishDate = (TextView) v.findViewById(R.id.tweet_item_date);
                this.userProfileImage = (ImageView) v.findViewById(R.id.profile_image);
                this.tweetImage = (ImageView) v.findViewById(R.id.tweet_item_image);
            }
        }

        public MyAdapter(List<Tweet> myDataset) {
            tweets = myDataset;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tweet_item, parent, false));
        }

        public List<Tweet> getData() {
            return tweets;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Tweet tweet = tweets.get(position);
            holder.title.setText(tweet.user.name);
            holder.tweetDescription.setText(tweet.text);
            holder.tweetPublishDate.setText(tweet.createdAt);
            if (!TextUtils.isEmpty(tweet.user.profileImageUrl))
                Glide.with(getContext()).load(tweet.user.profileImageUrl).into(holder.userProfileImage);
            if (tweet.entities.media != null && tweet.entities.media.size() > 0) {
                holder.tweetImage.setVisibility(View.VISIBLE);
                Glide.with(getContext()).load(tweet.entities.media.get(0).mediaUrl).into(holder.tweetImage);
            } else
                holder.tweetImage.setVisibility(View.GONE);
        }


        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return tweets.size();
        }

    }
}
